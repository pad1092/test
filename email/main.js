var btn = document.querySelector('.button')
var heading = document.querySelector('.heading');

btn.addEventListener("click", viec);

function viec(){
    var viecInterval = setInterval(function(){
        heading.classList.toggle('color');
    }, 500);
    setTimeout(function(){
        clearInterval(viecInterval);
    }, 5000);
}

setTimeout(function(){
    btn.removeEventListener('click', viec);
}, 10000);